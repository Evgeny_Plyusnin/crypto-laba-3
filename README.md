# Лабораторная работа №3



## Начало работы

Ранцевая криптосистема Меркла — Хеллмана, основанная на «задаче о рюкзаке», была разработана Ральфом Мерклом и Мартином Хеллманом в 1978 году. Это была одна из первых криптосистем с открытым ключом, но она оказалась криптографически нестойкой и, как следствие, не приобрела популярности.

Для того, чтобы поменять текст для шифрования, нужно использовать переменные окружения в *docker-compose.yml*

# Запуск

```
git clone https://gitlab.com/Evgeny_Plyusnin/crypto-laba-3.git
cd crypto-laba-3
docker-compose up
```

