import random
import os
from dotenv import load_dotenv


def euclid_extended(a: int, b: int):
    if b == 0:
        return a, 1, 0
    else:
        gcd, x1, y1 = euclid_extended(b, a % b)
    x = y1
    y = x1 - (a // b) * y1
    return gcd, x, y


def multiplicative_inverse(r, q):
    gcd, x, y = euclid_extended(r, q)
    return q + x


def generate_prime_number(a):
    count = 0
    for i in range(a + 1, 100000000000000):
        for j in range(1, i + 1):
            if i % j == 0:
                count += 1
            if count > 2:
                count = 0
                break
        if count == 2:
            return i


def generate_closed_key():
    w_set = {2, 7, 11, 21, 42, 89, 180, 354}
    w = list(sorted(w_set))

    w_sum = sum(w)
    q = generate_prime_number(w_sum)
    r = random.randint(1, q)

    b = list()
    for i in range(0, len(w)):
        b.append((w[i] * r) % q)

    # print(w, w_sum, q, r)
    # print(b)
    return w, b, q, r


def encode(text_array, b):
    sums = []
    for arr in text_array:
        a_sum = 0
        for i in range(0, len(arr)):
            if arr[i] == '1':
                a_sum += b[i]
        sums.append(a_sum)
    return sums


def decode(text_sum_array, w):
    mult_r_inverse = multiplicative_inverse(r, q)
    binary_text = []

    w.reverse()

    for binary_sum in text_sum_array:
        b_sum = (binary_sum * mult_r_inverse) % q

        res = list()

        for w_element in w:
            if w_element <= b_sum:
                b_sum -= w_element
                res.append('1')
            else:
                res.append('0')

        res.reverse()
        binary_text.append("".join(res))

    return ''.join([chr(int(x, 2)) for x in binary_text])


def generate_binary(text):
    return [bin(ord(x))[2:].zfill(8) for x in text]


load_dotenv()

text_for_encryption = os.getenv("TEXT", "Hello, World!")
text_for_encryption_binary = generate_binary(text_for_encryption)
print("Вход:", text_for_encryption)

w, b, q, r = generate_closed_key()
encrypted = encode(text_for_encryption_binary, b)
decoded = decode(encrypted, w)
print("Выход:", decoded)

