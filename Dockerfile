FROM python:3.12

WORKDIR crypto-laba
ENV TEXT "Hello!"

COPY main.py main.py

RUN pip install python-dotenv
CMD ["python", "main.py"]